<?php

namespace App\Controllers;

use App\Models\AdminModel;
use App\Models\ClassesModel;
use App\Models\ClassSubjectModel;
use App\Models\MarkModel;
use App\Models\StudentModel;
use App\Models\SubjectModel;
use App\Models\DepartmentModel;

class Home extends BaseController
{
	public function index()
	{
		session_start();
		if ($_SESSION['user']) {
			return redirect()->to(base_url() . '/home/classes');
		}
		return view('Home');
	}
	public function markByStudentID()
	{
		if (isset($_GET['maSV'])) {
			$maSV = $_GET['maSV'];
			$markModel = new MarkModel();
			$studentModel = new StudentModel();
			$subjectModel = new SubjectModel();
			$classesModel = new ClassesModel();
			$departmentModel = new DepartmentModel();
			$student = $studentModel->getStudentByCode($maSV)[0];
			$sid = $student["id"];
			$markAr = [];
			if ($sid !== null) {
				$mark = $markModel->getMarkByStudentID($sid);
				foreach ($mark as $key1 => $value) {
					$mark[$key1]['studentName'] = $studentModel->getStudentById($value['studentID'])[0]['name'];
					$mark[$key1]['studentCode'] = $studentModel->getStudentById($value['studentID'])[0]['studentCode'];
					$classIDS = unserialize($studentModel->getStudentById($value['studentID'])[0]['classID']);
					if (is_array($classIDS)) {
						foreach ($classIDS as $key2 => $classID) {
							$subID = $classesModel->getById($classID)[0]["subID"];
							$subName = $subjectModel->getSubjectById($subID)[0]["name"];
							$departmentName = $departmentModel->getDepartmentById($subID)[0]["name"];
							$marks = $markModel->getMarkByStudentIDAndSubID($sid, $classID)[0];
							$markAr[$key2]["classID"] = $classID;
							$markAr[$key2]["subID"] = $subID;
							$markAr[$key2]["subName"] = $subName;
							$markAr[$key2]["departmentName"] = $departmentName;
							$markAr[$key2]['gradeAttend'] = $marks['gradeAttend'] != null ? $marks['gradeAttend'] : "--";
							$markAr[$key2]['gradeMidTerm'] = $marks['gradeMidTerm'] != null ? $marks['gradeMidTerm'] : "--";
							$markAr[$key2]['gradeFinalTerm'] = $marks['gradeFinalTerm'] != null ? $marks['gradeFinalTerm'] : "--";
							$markAr[$key2]['grade'] = $marks['grade'] != null ? $marks['grade'] : "--";
							$grade = (float)$marks['grade'];
							$markAr[$key2]['gradeByTxt'] = 'F';
							if ($grade >= 8.5) {
								$markAr[$key2]['gradeByTxt'] = 'A';
							}
							else if ($grade >= 8 && $grade < 8.5) {
								$markAr[$key2]['gradeByTxt'] = 'B+';
							}
							else if ($grade >= 7) {
								$markAr[$key2]['gradeByTxt'] = 'B';
							}
							else if ($grade >= 6) {
								$markAr[$key2]['gradeByTxt'] = 'C+';
							}
							else if ($grade >= 5.5) {
								$markAr[$key2]['gradeByTxt'] = 'C';
							}
							else if ($grade >= 5) {
								$markAr[$key2]['gradeByTxt'] = 'D+';
							}
							else if ($grade >= 4.5) {
								$markAr[$key2]['gradeByTxt'] = 'D';
							}
							$markAr[$key2]['isPass'] = (float)$markAr[$key2]['gradeAttend'] < 5 || $markAr[$key2]['gradeByTxt'] == 'F' ? 'Trượt' : 'Đạt';
						}
					}
				}
				$data['mark'] = $mark;
				// var_dump($markAr);
				$data['markAr'] = $markAr;
				return view('mark', $data);
			}
			$data['error'] = 'Không tồn tại.';
			return view('getMark', $data);
		}
	}
	public function getMark()
	{
		return view('getMark');
	}
	public function classes()
	{
		$data = [];
		$classesModel = new ClassesModel();
		$classSubjectModel = new ClassSubjectModel();
		$studentModel = new StudentModel();
		$subjectModel = new SubjectModel();
		$markModel = new MarkModel();
		session_start();
		$adminID = $_SESSION['user']['id'];
		$classes = $classesModel->getByTeacherID($adminID);

		$classIDS = [];
		$subjectIDS = [];
		// foreach ($classSubject as $key => $value) {
		// 	$classIDS[$key] = $classesModel->getById($value['teacherID'])[0]['id'];
		// 	// $subjectIDS[$key] = $subjectModel->getSubjectById($value['subID'])[0]['id'];
		// }
		// $classIDS = array_unique($classIDS);
		// $subjectIDS = array_unique($subjectIDS);
		$data['classes'] = $classes;

		// $data['subjects'] = [];
		// foreach ($classIDS as $key => $value) {
		// 	$class = $classesModel->getById($value);
		// 	$data['classes'][$key] = [
		// 		'id' => $class[0]['id'],
		// 		'name' => $class[0]['name']
		// 	];
		// }
		// foreach ($subjectIDS as $key => $value) {
		// 	$subject = $subjectModel->getSubjectById($value);
		// 	$data['subjects'][$key] = [
		// 		'id' => $subject[0]['id'],
		// 		'name' => $subject[0]['name']
		// 	];
		// }
		if ($_SESSION['role'] == '1') {
			$data['classes'] = $classesModel->getAll();
			// $data['subjects'] = $subjectModel->getAll();
		}
		if (isset($_GET['classID'])) {
			$classID = $_GET['classID'];
			// $subjectID  = $_GET['subjectID'];
			$students = $studentModel->getAll();
			$studentbyClass = [];
			foreach ($students as $key => $value) {
				$classIDS = unserialize($value["classID"]);
				if (is_array($classIDS)) {
					foreach ($classIDS as $item) {
						if ($item == $classID) {
							$value['className'] = $classesModel->getById($classID)[0]['name'];
							$mark = $markModel->getMarkByStudentIDAndSubID($value['id'], $classID)[0];
							$value['gradeAttend'] = $mark['gradeAttend'] != null ? $mark['gradeAttend'] : "--";
							$value['gradeMidTerm'] = $mark['gradeMidTerm'] != null ? $mark['gradeMidTerm'] : "--";
							$value['gradeFinalTerm'] = $mark['gradeFinalTerm'] != null ? $mark['gradeFinalTerm'] : "--";
							$value['grade'] = $mark['grade'] != null ? $mark['grade'] : "--";
							$grade = (float)$mark['grade'];
							$value['gradeByTxt'] = 'F';
							if ($grade >= 8.5) {
								$value['gradeByTxt'] = 'A';
							}
							else if ($grade >= 8 && $grade < 8.5) {
								$value['gradeByTxt'] = 'B+';
							}
							else if ($grade >= 7) {
								$value['gradeByTxt'] = 'B';
							}
							else if ($grade >= 6) {
								$value['gradeByTxt'] = 'C+';
							}
							else if ($grade >= 5.5) {
								$value['gradeByTxt'] = 'C';
							}
							else if ($grade >= 5) {
								$value['gradeByTxt'] = 'D+';
							}
							else if ($grade >= 4.5) {
								$value['gradeByTxt'] = 'D';
							}
							$value['isPass'] = (float)$value['gradeAttend'] < 5 || $value['gradeByTxt'] == 'F' ? 'Trượt' : 'Đạt';
							array_push($studentbyClass, $value);
						}
					}
				}
			}
			$data['currentClass'] = $classesModel->getById($classID)[0]['name'];

			// $data['currentSubject']  = $subjectModel->getSubjectById($subjectID)[0]['name'];

			// var_dump($students);
		}
		// var_dump($studentbyClass, count($studentbyClass));
		$data['students'] = $studentbyClass;
		return view('classes', $data);
	}
	public function editMark()
	{
		$markModel = new MarkModel();
		$data['mark'] = [];
		if (isset($_GET['studentID']) && isset($_GET['classID'])) {
			$studentID = $_GET['studentID'];
			$classID = $_GET['classID'];
			$mark = $markModel->getMarkByStudentIDAndSubID($studentID, $classID)[0];
			$data['mark'] = $mark;
		}
		if ($this->request->getMethod() == 'post') {
			if (isset($_GET['studentID']) && isset($_GET['classID'])) {
				$markModel = new MarkModel();
				$classesModel = new ClassesModel();
				$studentID = $_GET['studentID'];
				$classID = $_GET['classID'];
				$gradeAttend = (float)$this->request->getVar('gradeAttend');
				$gradeMidTerm = (float)$this->request->getVar('gradeMidTerm');
				$gradeFinalTerm = (float)$this->request->getVar('gradeFinalTerm');
				if ($gradeAttend < 5) {
					return '<script>alert("Điểm chuyên cần dưới 5 không thể sửa điểm");window.location.href="/nhom6/home/classes";</script>';
				}
				$class = $classesModel->getById($classID);
				if (isset($class[0]['id'])) {
					if (strtotime($class[0]['timeEnd']) < time()) {
						return '<script>alert("Lớp học đã kết thúc");window.location.href="/nhom6/home/classes";</script>';
					}
				}
				$grades = [$gradeAttend, $gradeMidTerm, $gradeFinalTerm];
				$grade = number_format($gradeAttend * 0.1 + $gradeMidTerm * 0.2 + $gradeFinalTerm * 0.7, 2);
				$checkMark = $markModel->getMarkByStudentIDAndSubID($studentID, $classID)[0]['id'];
				session_start();
				$username = $_SESSION['user']['username'];
				if ($checkMark > 0) {
					$data_insert = [
						'id' => $checkMark,
						'classID' => $classID,
						'studentID' => $studentID,
						'gradeAttend' => $gradeAttend,
						'gradeMidTerm' => $gradeMidTerm,
						'gradeFinalTerm' => $gradeFinalTerm,
						'grade' => $grade,
						'modifiedBy' => $username,
						'modifiedDate' => date("Y-m-d h:i:s"),
					];
				} else {
					$data_insert = [
						'classID' => $classID,
						'studentID' => $studentID,
						'gradeAttend' => $gradeAttend,
						'gradeMidTerm' => $gradeMidTerm,
						'gradeFinalTerm' => $gradeFinalTerm,
						'grade' => $grade,
						'modifiedBy' => $username,
						'modifiedDate' => date("Y-m-d h:i:s"),
					];
				}
				$newMark = $markModel->save($data_insert);
				if ($newMark > 0) {
					return redirect()->to(base_url() . '/home/classes?classID=' . $classID);
				} else {
					echo '<script>alert("Đã xảy ra lỗi!!!");</script>';
				}
			}
		}
		return view('editMark', $data);
	}
	public function deleteMark()
	{
		if (isset($_GET['studentID']) && isset($_GET['classID'])) {
			$markModel = new MarkModel();
			$studentID = $_GET['studentID'];
			$classID = $_GET['classID'];
			$checkMark = $markModel->getMarkByStudentIDAndSubID($studentID, $classID)[0]['id'];
			$data_insert = [
				'id' => $checkMark,
				'gradeAttend' => null,
				'gradeMidTerm' => null,
				'gradeFinalTerm' => null,
				'grade' => null,
				'modifiedDdate' => date("Y-m-d h:i:s"),
			];
			$newMark = $markModel->save($data_insert);
			if ($newMark > 0) {
				return redirect()->to(base_url() . '/home/classes');
			}
		}
	}
	public function class()
	{
		$subjectModel = new SubjectModel();
		$classesModel = new ClassesModel();
		$adminModel = new AdminModel();
		$classes = $classesModel->getAll();
		$subjects = $subjectModel->getAll();
		if (isset($_GET['subjectID'])) {
			$subjectID = $_GET['subjectID'];
			if ($subjectID != '') {
				$classes = $classesModel->getBySubjectID($subjectID);
			}
		}
		foreach ($classes as $key => $value) {
			$classes[$key]['teacherName'] = $adminModel->getById($classes[$key]['teacherID'])[0]['name'];
		}
		$data['classes'] = $classes;
		$data['subjects'] = $subjects;
		return view('class', $data);
	}
	public function editClass()
	{
		$classesModel = new ClassesModel();
		$adminModel = new AdminModel();
		$subjectModel = new SubjectModel();
		$departmentModel = new DepartmentModel();
		$departments = $departmentModel->getAll();
		$data['class'] = [];
		$data['teachers'] = $adminModel->getAll();
		$data['subjects'] = $subjectModel->getAll();
		$data['departments'] = $departmentModel->getAll();
		if (isset($_GET['classID'])) {
			$classID = $_GET['classID'];
			$class = $classesModel->getById($classID);
			$subID = $class[0]['subID'];
			$subject = $subjectModel->getSubjectById($subID);
			$class[0]['departmentID'] = $subject[0]['departmentID'];
			$data['class'] = $class[0];
		}
		if ($this->request->getMethod() == 'post') {
			$code = $this->request->getVar('code');
			$name = $this->request->getVar('name');
			$numberStudent = $this->request->getVar('numberStudent');
			$teacher = $this->request->getVar('teacher');
			$subID = $this->request->getVar('subID');
			$timeStart = $this->request->getVar('timeStart');
			$timeEnd = $this->request->getVar('timeEnd');
			$checkClass = $classesModel->getById($classID)[0]['id'];
			if ($classID == null) {
				$data_insert = [
					'code' => $code,
					'name' => $name,
					'numberStudent' => $numberStudent,
					'subID' => $subID,
					'teacherID' => $teacher,
					'timeStart' => $timeStart,
					'timeEnd' => $timeEnd,
					'createdDate' => date("Y-m-d h:i:s"),
				];
			} else {
				$data_insert = [
					'id' => $checkClass,
					'code' => $code,
					'name' => $name,
					'numberStudent' => $numberStudent,
					'subID' => $subID,
					'teacherID' => $teacher,
					'timeStart' => $timeStart,
					'timeEnd' => $timeEnd,
					'modifiedDate' => date("Y-m-d h:i:s"),
				];
			}
			$newClass = $classesModel->save($data_insert);
			if ($newClass > 0) {
				return redirect()->to(base_url() . '/home/class');
			} else {
				echo '<script>alert("Đã xảy ra lỗi!!!");</script>';
			}
		}
		return view('editClass', $data);
	}
	public function deleteClass()
	{
		if (isset($_GET['classID'])) {
			$classesModel = new ClassesModel();
			$studentModel = new StudentModel();
			$classID = $_GET['classID'];
			$lstStudent = $studentModel->getAll();
			foreach ($lstStudent as $student) {
				$classIDS = unserialize($student["classID"]);
				if (is_array($classIDS)) {
					foreach ($classIDS as $id) {
						if ($classID == $id) {
							echo "<script>alert('Vẫn còn học sinh trong lớp, không thể xoá!!');</script>";
							$classesModel = new ClassesModel();
							$adminModel = new AdminModel();
							$classes = $classesModel->getAll();
							foreach ($classes as $key => $value) {
								$classes[$key]['teacherName'] = $adminModel->getById($classes[$key]['teacherID'])[0]['name'];
							}
							$data['classes'] = $classes;
							return view('class', $data);
						}
					}
				}
			}
			$oldMakr = $classesModel->where('id', $classID)->delete();
			if ($oldMakr > 0) {
				return redirect()->to(base_url() . '/home/class');
			}
		}
	}
	public function subject()
	{
		$subjectModel = new SubjectModel();
		$departmentModel = new DepartmentModel();
		$subjects = $subjectModel->getAll();
		$departments = $departmentModel->getAll();
		if (isset($_GET['departmentID'])) {
			$departmentID = $_GET['departmentID'];
			if ($departmentID != '') {
				$subjects = $subjectModel->getByDepartmentID($departmentID);
			}
		}
		foreach ($subjects as $key => $value) {
			$departmentName = $departmentModel->getDepartmentById($subjects[$key]['departmentID'])[0]['name'];
			$subjects[$key]["departmentName"] = $departmentName;
		}
		$data['subjects'] = $subjects;
		$data['departments'] = $departments;
		return view('subject', $data);
	}
	public function editSubject()
	{
		$subjectModel = new SubjectModel();
		$departmentModel = new DepartmentModel();
		$departments = $departmentModel->getAll();
		$data['departments'] = $departments;
		$data['subject'] = [];

		if (isset($_GET['subjectID'])) {
			$subjectID = $_GET['subjectID'];
			$subject = $subjectModel->getSubjectById($subjectID);
			$data['subject'] = $subject[0];
		}
		if ($this->request->getMethod() == 'post') {
			$name = $this->request->getVar('name');
			$departmentID = $this->request->getVar('departmentID');
			$checkClass = $subjectModel->getSubjectById($subjectID)[0]['id'];
			if ($subjectID == null) {
				$data_insert = [
					'name' => $name,
					'departmentID' => $departmentID,
					'createdDate' => date("Y-m-d h:i:s"),
				];
			} else {
				$data_insert = [
					'id' => $checkClass,
					'name' => $name,
					'departmentID' => $departmentID,
					'modifiedDate' => date("Y-m-d h:i:s"),
				];
			}
			$newSubject = $subjectModel->save($data_insert);
			if ($newSubject > 0) {
				return redirect()->to(base_url() . '/home/subject');
			} else {
				echo '<script>alert("Đã xảy ra lỗi!!!");</script>';
			}
		}
		return view('editSubject', $data);
	}
	public function deleteSubject()
	{
		if (isset($_GET['subjectID'])) {
			$subjectModel = new SubjectModel();
			$classesModel = new ClassesModel();
			$subjectID = $_GET['subjectID'];
			$lstClass = $classesModel->getAll();
			foreach ($lstClass as $class) {
					if ($class["subID"] == $subjectID) {
						return "<script>alert('Vẫn tồn tại lớp học môn này, không thể xoá!!');window.location.href='/nhom6/home/subject';</script>";
						// $classesModel = new ClassesModel();
						// $subjectModel = new SubjectModel();
						// $subjects = $subjectModel->getAll();
						// $data['subjects'] = $subjects;
						// return view('subject', $data);
				}
			}
			$oldSubject = $subjectModel->where('id', $subjectID)->delete();
			if ($oldSubject > 0) {
				return redirect()->to(base_url() . '/home/subject');
			}
		}
	}
	public function department()
	{
		$departmentModel = new DepartmentModel();
		$departments = $departmentModel->getAll();
		$data['departments'] = $departments;
		return view('department', $data);
	}
	public function editDepartment()
	{
		$departmentModel = new DepartmentModel();
		$data['department'] = [];

		if (isset($_GET['departmentID'])) {
			$departmentID = $_GET['departmentID'];
			$department = $departmentModel->getDepartmentById($departmentID);
			$data['department'] = $department[0];
		}
		if ($this->request->getMethod() == 'post') {
			$name = $this->request->getVar('name');
			$checkClass = $departmentModel->getDepartmentById($departmentID)[0]['id'];
			if ($departmentID == null) {
				$data_insert = [
					'name' => $name,
					'createdDate' => date("Y-m-d h:i:s"),
				];
			} else {
				$data_insert = [
					'id' => $checkClass,
					'name' => $name,
					'modifiedDate' => date("Y-m-d h:i:s"),
				];
			}
			$newDepartment = $departmentModel->save($data_insert);
			if ($newDepartment > 0) {
				return redirect()->to(base_url() . '/home/department');
			} else {
				echo '<script>alert("Đã xảy ra lỗi!!!");</script>';
			}
		}
		return view('editDepartment', $data);
	}
	public function deleteDepartment()
	{
		if (isset($_GET['departmentID'])) {
			$subjectModel = new SubjectModel();
			$departmentID = $_GET['departmentID'];
			$lstSubject = $subjectModel->getAll();
			foreach ($lstSubject as $subject) {
					if ($subject["id"] == $departmentID) {
						return "<script>alert('Vẫn còn môn trong khoa, không thể xoá!!');window.location.href='/nhom6/home/department';</script>";
						// $departmentModel = new DepartmentModel();
						// $departments = $departmentModel->getAll();
						// $data['departments'] = $subjects;
						// return view('department', $data);
				}
			}
			$oldSubject = $subjectModel->where('id', $subjectID)->delete();
			if ($oldSubject > 0) {
				return redirect()->to(base_url() . '/home/subject');
			}
		}
	}
	public function teacher()
	{
		$adminModel = new AdminModel();
		$subjectModel = new SubjectModel();
		$departmentModel = new DepartmentModel();
		$teachers = $adminModel->getAll();
		$departments = $departmentModel->getAll();
		if (isset($_GET['departmentID'])) {
			$departmentID = $_GET['departmentID'];
			if ($departmentID != '') {
				$subjects = $subjectModel->getByDepartmentID($departmentID);
				$teachersTemp = [];
				foreach ($subjects as $subject) {
					foreach ($teachers as $index => $teacher) {
						$subIDS = unserialize($teacher['subID']);
						if (is_array($subIDS)) {
							if (in_array($subject["id"], $subIDS)) {
								array_push($teachersTemp, $teacher);
							}
						}
					}
				}
				$teachers = $teachersTemp;
			}
		}
		foreach ($teachers as $key => $value) {
			$subIDS = unserialize($value['subID']);
			$teachers[$key]['subjectsName'] = "";
			if (is_array($subIDS)) {
				foreach ($subIDS as $index => $subID) {
					if ($index > 0) {
						$teachers[$key]['subjectsName'] = $teachers[$key]['subjectsName'] . ", " . $subjectModel->getSubjectById($subID)[0]['name'];
					} else {
						$teachers[$key]['departmentID'] = $subjectModel->getSubjectById($subID)[0]['departmentID'];
						$teachers[$key]['departmentName'] = $departmentModel->getDepartmentById($teachers[$key]['departmentID'])[0]['name'];
						$teachers[$key]['subjectsName'] = $teachers[$key]['subjectsName'] . $subjectModel->getSubjectById($subID)[0]['name'];
					}
				}
			}
		}
		$data['teachers'] = $teachers;
		$data['departments'] = $departments;
		return view('teacher', $data);
	}
	public function editTeacher()
	{
		$teacherID = null;
		$adminModel = new AdminModel();
		$subjectModel = new SubjectModel();
		$departmentModel = new DepartmentModel();
		$data['departments'] = $departmentModel->getAll();
		$data['teacher'] = [];
		if (isset($_GET['id'])) {
			$teacherID = $_GET['id'];
			$teacher = $adminModel->getById($teacherID);
			$teacher[0]['subIDS'] = unserialize($teacher[0]['subID']);
			$data['teacher'] = $teacher[0];
		}
		if (isset($_GET['departmentID'])) {
			$departmentID = $_GET['departmentID'];
			$subjects = $subjectModel->getByDepartmentID($departmentID);
			$data['subjects'] = $subjects;
		}

		if ($this->request->getMethod() == 'post') {
			$fullname = $this->request->getVar('fullname');
			$username = $this->request->getVar('username');
			$password = $this->request->getVar('password');
			$rePassword = $this->request->getVar('rePassword');
			$gender = $this->request->getVar('gender');
			$email = $this->request->getVar('email');
			$subjects = $this->request->getVar('subjects');
			$birthday = $this->request->getVar('birthday');
			$phonenumber = $this->request->getVar('phonenumber');
			$address = $this->request->getVar('address');


			$data['error'] = [];
			$checkUsername = $adminModel->where('username', $username)->countAllResults();
			$checkEmail = $adminModel->where('email', $email)->countAllResults();
			$checkPassword = $password != $rePassword;
			$txtError = '';

			$checkTeacher = null;
			if (isset($_GET['id'])) {
				$teacherID = $_GET['id'];
				$conditionUsername = [
					'username' => $username,
					'id!=' => $teacherID
				];
				$conditionEmail = [
					'email' => $email,
					'id!=' => $teacherID
				];
				$checkUsername = $adminModel->where($conditionUsername)->countAllResults();
				$checkEmail = $adminModel->where($conditionEmail)->countAllResults();
				$checkTeacher = $adminModel->getById($teacherID)[0]['id'];
				$data_insert = [
					'id' => $checkTeacher,
					'name' => $fullname,
					'username' => $username,
					'gender' => $gender,
					'birthday' => $birthday,
					'phoneNumber' => $phonenumber,
					'email' => $email,
					'address' => $address,
					'password' => $password,
					'subID' => serialize($subjects),
					'modifiedDate' => date("Y-m-d h:i:s"),
					'status' => 1,
					'role' => 0
				];
			} else {
				$data_insert = [
					'name' => $fullname,
					'username' => $username,
					'gender' => $gender,
					'birthday' => $birthday,
					'phoneNumber' => $phonenumber,
					'email' => $email,
					'address' => $address,
					'password' => $password,
					'subID' => serialize($subjects),
					'createdDate' => date("Y-m-d h:i:s"),
					'status' => 1,
					'role' => 0
				];
			}
			if ($checkPassword || $checkUsername || $checkEmail) {
				$data['message'] = 'fail';
				if ($checkPassword) {
					$txtError = '2 mật khẩu không trùng khớp !';
					array_push($data['error'], $txtError);
				}
				if ($checkUsername != 0) {
					$txtError = 'Đã có người đăng ký username này, vui lòng nhập tên username khác !';
					array_push($data['error'], $txtError);
				}
				if ($checkEmail != 0) {
					$txtError = 'Đã có người đăng ký email này, vui lòng nhập tên email khác !';
					array_push($data['error'], $txtError);
				}
				return view('editTeacher', $data);
			}
			$newClass = $adminModel->save($data_insert);
			if ($newClass > 0) {
				return redirect()->to(base_url() . '/home/teacher');
			} else {
				echo '<script>alert("Đã xảy ra lỗi!!!");</script>';
			}
		}
		return view('editTeacher', $data);
	}
	public function deleteTeacher()
	{
		if (isset($_GET['id'])) {
			$adminModel = new AdminModel();
			$teacherID = $_GET['id'];
			$old = $adminModel->where('id', $teacherID)->delete();
			if ($old > 0) {
				return redirect()->to(base_url() . '/home/teacher');
			}
		}
	}
	public function classSubject()
	{
		$data = [];
		$classSubjectModel = new ClassSubjectModel();
		$classesModel = new ClassesModel();
		$subjectModel = new SubjectModel();
		$adminModel = new AdminModel();

		$classSubject = $classSubjectModel->getAll();
		$result = [];
		foreach ($classSubject as $key => $value) {
			$result[$key]['id'] = $value['id'];
			$result[$key]['class']['className'] = $classesModel->getById($value['classID'])[0]['name'];
			$result[$key]['class']['classID'] = $value['classID'];
			$result[$key]['subject']['subjectName'] = $subjectModel->getSubjectById($value['subID'])[0]['name'];
			$result[$key]['subject']['subjectID'] = $value['subID'];
			$result[$key]['teacher']['teacherName'] = $adminModel->getById($value['adminID'])[0]['name'];
			$result[$key]['teacher']['teacherID'] = $value['adminID'];
		}
		$data['result'] = $result;
		// var_dump($result);
		return view('classSubject', $data);
	}
	public function editClassSubject()
	{
		$classSubjectModel = new ClassSubjectModel();
		$classesModel = new ClassesModel();
		$subjectModel = new SubjectModel();
		$adminModel = new AdminModel();
		$data['classSubjects'] = [];
		$classes = $classesModel->getAll();
		$subjects = $subjectModel->getAll();
		$teachers = $adminModel->getAll();
		$data['classes'] = $classes;
		$data['subjects'] = $subjects;
		$data['teachers'] = $teachers;
		if (isset($_GET['id'])) {
			$id = $_GET['id'];
			$classSubject = $classSubjectModel->getById($id);
			$classID = $classSubject[0]['classID'];
			$subjectID = $classSubject[0]['subID'];
			$teacherID = $classSubject[0]['adminID'];
			$class = $classesModel->getById($classID);
			$subject = $subjectModel->getSubjectById($subjectID);
			$teacher = $adminModel->getById($teacherID);


			$data['classID'] = $classID;
			$data['subjectID'] = $subjectID;
			$data['teacherID'] = $teacherID;

			$data['class'] = $class[0];
			$data['subject'] = $subject[0];
			$data['teacher'] = $teacher[0];
		}
		if ($this->request->getMethod() == 'post') {
			$teacher = $this->request->getVar('teacher');
			$class = $this->request->getVar('class');
			$subject = $this->request->getVar('subject');
			$classSubjects = $classSubjectModel->getAll();
			foreach ($classSubjects as $item) {
				if ($item['adminID'] == $teacher && $item['subID'] == $subject && $item['classID'] == $class) {
					$data['error'] = 'Đã tồn tại bộ môn này';
					return view('editClassSubject', $data);
				}
			}
			if (isset($_GET['id'])) {
				$id = $_GET['id'];
				$data_insert = [
					'id' => $id,
					'adminID' => $teacher,
					'subID' => $subject,
					'classID' => $class,
				];
			} else {
				$data_insert = [
					'adminID' => $teacher,
					'subID' => $subject,
					'classID' => $class,
				];
			}
			$new = $classSubjectModel->save($data_insert);
			if ($new > 0) {
				return redirect()->to(base_url() . '/home/classSubject');
			} else {
				echo '<script>alert("Đã xảy ra lỗi!!!");</script>';
			}
		}
		return view('editClassSubject', $data);
	}
	public function deleteClassSubject()
	{
		if (isset($_GET['id'])) {
			$classSubjectModel = new ClassSubjectModel();
			$id = $_GET['id'];
			$old = $classSubjectModel->where('id', $id)->delete();
			if ($old > 0) {
				return redirect()->to(base_url() . '/home/classSubject');
			}
		}
	}
	public function students()
	{
		$studentModel = new StudentModel();
		$departmentModel = new DepartmentModel();
		$students = $studentModel->getAll();
		$departments = $departmentModel->getAll();
		if (isset($_GET['departmentID'])) {
			$departmentID = $_GET['departmentID'];
			if ($departmentID != '') {
				$students = $studentModel->getByDepartmentID($departmentID);
			}
		}
		foreach ($students as $key => $value) {
			$departmentName = $departmentModel->getDepartmentById($students[$key]['departmentID'])[0]['name'];
			$departmentID = $departmentModel->getDepartmentById($students[$key]['departmentID'])[0]['id'];
			$students[$key]["departmentName"] = $departmentName;
			$students[$key]["departmentID"] = $departmentID;
		}
		$data['students'] = $students;
		$data['departments'] = $departments;
		return view('student', $data);
	}
	public function editStudent()
	{
		$studentModel = new StudentModel();
		$classesModel = new ClassesModel();
		$subjectModel = new SubjectModel();
		$departmentModel = new DepartmentModel();
		$departments = $departmentModel->getAll();
		$classes = $classesModel->getAll();
		// $data['classes'] = $classes;
		$data['student'] = [];
		$data['departments'] = $departments;

		if (isset($_GET['id'])) {
			$studentID = $_GET['id'];
			$student = $studentModel->getStudentById($studentID);
			$student[0]["class"] = unserialize($student[0]['classID']);
			$data['student'] = $student[0];
		}
		if (isset($_GET['departmentID'])) {
			$departmentID = $_GET['departmentID'];
			$lstClass = [];
			$subjects = $subjectModel->getByDepartmentID($departmentID);
			foreach ($subjects as $key => $subject) {
				$subID = $subject["id"];
				foreach ($classes as $key => $class) {
					if ($class["subID"] == $subID) {
						array_push($lstClass, $class);
					}
				}
			}
			$data['classes'] = $lstClass;
		}

		if ($this->request->getMethod() == 'post') {
			$fullname = $this->request->getVar('fullname');
			$username = $this->request->getVar('username');
			$studentCode = $this->request->getVar('studentCode');
			$classID = $this->request->getVar('classID');
			$password = $this->request->getVar('password');
			$rePassword = $this->request->getVar('rePassword');
			$gender = $this->request->getVar('gender');
			$email = $this->request->getVar('email');
			$birthday = $this->request->getVar('birthday');
			$phonenumber = $this->request->getVar('phonenumber');
			$address = $this->request->getVar('address');


			$data['error'] = [];
			$checkUsername = $studentModel->where('username', $username)->countAllResults();
			$checkEmail = $studentModel->where('email', $email)->countAllResults();
			$checkStudentCode = $studentModel->where('studentCode', $studentCode)->countAllResults();
			$checkPassword = $password != $rePassword;
			$txtError = '';
			$students = $studentModel->getAll();
			$tickClass = $classesModel->getByIds($classID);
			foreach ($tickClass as $key => $item1) {
				$tickClass[$key]['count'] = 0;
				foreach ($students as $student) {
					$classIDS = unserialize($student['classID']);
					if (is_array($classIDS)) {
						foreach ($classIDS as $item2) {
							if ($item2 == $tickClass[$key]["id"]) {
								$tickClass[$key]['count']++;
							}
						}
					}
				}
			}
			$fullClassAr = [];
			foreach ($tickClass as $key => $class) {
				$maxStudent = $classesModel->getById($class["id"])[0]['numberStudent'];
				if ($class["count"] >= $maxStudent) {
					$name = $classesModel->getById($class["id"])[0]['name'];
					array_push($fullClassAr, $name);
				}
			}
			$fullClassName = "";
			foreach ($fullClassAr as $key => $item) {
				if ($key == 0) {
					$fullClassName = $item;
				} else {
					$fullClassName = $fullClassName . "," . $item;
				}
			}
			$checkStudent = null;
			if (isset($_GET['id'])) {
				$studentID = $_GET['id'];
				$conditionUsername = [
					'username' => $username,
					'id!=' => $studentID
				];
				$conditionEmail = [
					'email' => $email,
					'id!=' => $studentID
				];
				$conditionCode = [
					'studentCode' => $studentCode,
					'id!=' => $studentID
				];
				$checkUsername = $studentModel->where($conditionUsername)->countAllResults();
				$checkEmail = $studentModel->where($conditionEmail)->countAllResults();
				$checkStudentCode = $studentModel->where($conditionCode)->countAllResults();
				$checkStudent = $studentModel->getStudentById($studentID)[0]['id'];

				foreach ($tickClass as $key2 => $item1) {
					$tickClass[$key2]['count'] = 0;
					foreach ($students as $student) {
						if ($student['id'] != $studentID) {
							$classIDS = unserialize($student['classID']);
							if (is_array($classIDS)) {
								foreach ($classIDS as $item2) {
									if ($item2 == $tickClass[$key2]["id"]) {
										$tickClass[$key2]['count']++;
									}
								}
							}
						}
					}
				}
				$fullClassAr = [];
				foreach ($tickClass as $key => $class) {
					$maxStudent = $classesModel->getById($class["id"])[0]['numberStudent'];
					if ($class["count"] >= $maxStudent) {
						$name = $classesModel->getById($class["id"])[0]['name'];
						array_push($fullClassAr, $name);
					}
				}
				$fullClassName = "";
				foreach ($fullClassAr as $key => $item) {
					if ($key == 0) {
						$fullClassName = $item;
					} else {
						$fullClassName = $fullClassName . "," . $item;
					}
				}
				$checkSubjectDuplicate = '';
				if (is_array($classID)) {
					$classed = $classesModel->getByIds($classID);
					foreach ($classed as $key => $class) {
						if ($classed[$key]["subID"] == $classed[$key+1]["subID"]) {
							$subName = $subjectModel->getSubjectById($classed[$key]["subID"])[0]["name"];
							$checkSubjectDuplicate = "Đã đăng ký môn ". $subName;
						}
					}
				}
				$checkClassIsStartOrEnd = '';
				if (is_array($classID)) {
					$classed = $classesModel->getByIds($classID);
					foreach ($classed as $key => $class) {
						$timeStart = $class["timeStart"];
						$timeEnd = $class["timeEnd"];
						if (strtotime($timeStart) < time() || strtotime($timeEnd) < time()) {
							$checkClassIsStartOrEnd = 'Lớp học '. $class["name"] .' đã bắt đầu hoặc kết thúc học phần.';
						}
					}
				}

				$data_insert = [
					'id' => $checkStudent,
					'name' => $fullname,
					'classID' => serialize($classID),
					'departmentID' => $departmentID,
					'studentCode' => $studentCode,
					'username' => $username,
					'gender' => $gender,
					'birthday' => $birthday,
					'phoneNumber' => $phonenumber,
					'email' => $email,
					'address' => $address,
					'password' => $password,
					'modifiedDate' => date("Y-m-d h:i:s"),
					'status' => 1,
				];
			} else {
				$data_insert = [
					'name' => $fullname,
					'classID' => serialize($classID),
					'departmentID' => $departmentID,
					'studentCode' => $studentCode,
					'username' => $username,
					'gender' => $gender,
					'birthday' => $birthday,
					'phoneNumber' => $phonenumber,
					'email' => $email,
					'address' => $address,
					'password' => $password,
					'createdDate' => date("Y-m-d h:i:s"),
					'status' => 1,
				];
			}

			if ($checkPassword || $checkUsername || $checkEmail || $checkStudentCode || $fullClassName || $checkSubjectDuplicate || $checkClassIsStartOrEnd) {
				$data['message'] = 'fail';
				if ($checkPassword) {
					$txtError = '2 mật khẩu không trùng khớp !';
					array_push($data['error'], $txtError);
				}
				if ($checkUsername != 0) {
					$txtError = 'Đã có người đăng ký username này, vui lòng nhập tên username khác !';
					array_push($data['error'], $txtError);
				}
				if ($checkEmail != 0) {
					$txtError = 'Đã có người đăng ký email này, vui lòng nhập tên email khác !';
					array_push($data['error'], $txtError);
				}
				if ($checkStudentCode != 0) {
					$txtError = 'Đã tồn tại mã sinh viên.';
					array_push($data['error'], $txtError);
				}
				if ($fullClassName != "") {
					$txtError = 'Lớp ' . $fullClassName . ' đã đầy.';
					array_push($data['error'], $txtError);
				}
				if ($checkSubjectDuplicate != "") {
					array_push($data['error'], $checkSubjectDuplicate);
				}
				if ($checkClassIsStartOrEnd != "") {
					array_push($data['error'], $checkClassIsStartOrEnd);
				}
				return view('editStudent', $data);
			}
			$newClass = $studentModel->save($data_insert);
			if ($newClass > 0) {
				return redirect()->to(base_url() . '/home/students');
			} else {
				echo '<script>alert("Đã xảy ra lỗi!!!");</script>';
			}
		}

		return view('editStudent', $data);
	}
	public function deleteStudent()
	{
		if (isset($_GET['id'])) {
			$studentModel = new StudentModel();
			$teacherID = $_GET['id'];
			$old = $studentModel->where('id', $teacherID)->delete();
			if ($old > 0) {
				return redirect()->to(base_url() . '/home/students');
			}
		}
	}
	public function PhanQuyen()
	{
		$adminModel = new AdminModel();
		$allAdmin = $adminModel->getAllAdmin();
		$data['AllAdmin'] = $allAdmin;
		return view('PhanQuyen', $data);
	}
	public function editPhanQuyen()
	{
		if ($this->request->getMethod() == 'post') {
			$role = $this->request->getVar('role');

			if (isset($_GET['id'])) {
				$id = $_GET['id'];
				$adminModel = new AdminModel();
				$data_insert = [
					'id' => $id,
					'role' => $role
				];
				$new = $adminModel->save($data_insert);
				if ($new > 0) {
					return redirect()->to(base_url() . '/home/PhanQuyen');
				} else {
					echo '<script>alert("Đã xảy ra lỗi!!!");</script>';
				}
			}
		}
		return view('editPhanQuyen');
	}
	public function getTeacherBySubject($sid = null)
	{
		$adminModel = new AdminModel();
		$subjectModel = new SubjectModel();
		$subID = $subjectModel->getSubjectById($sid);
		$teachers = $adminModel->getAll();
		$teachersBySubject = [];
		foreach ($teachers as $key => $teacher) {
			$subIDS = unserialize($teacher["subID"]);
			if (is_array($subIDS)) {
				foreach ($subIDS as $key2 => $subID) {
					if ($subID == $sid) {
						array_push($teachersBySubject, $teacher);
					}
				}
			}
		}
		$json = json_encode($teachersBySubject);
		return $json;
	}
	public function getClassByDepartment($id = null)
	{
		$subjectModel = new SubjectModel();
		$departmentModel = new DepartmentModel();
		$classesModel = new ClassesModel();
		$lstClass = [];
		$subjects = $subjectModel->getByDepartmentID($id);
		$classes = $classesModel->getAll();
		foreach ($subjects as $key => $subject) {
			$subID = $subject["id"];
			foreach ($classes as $key => $class) {
				if ($class["subID"] == $subID) {
					array_push($lstClass, $class);
				}
			}
		}
		$json = json_encode($lstClass);
		return $json;
	}
	public function getSubjectByDepartment($id = null)
	{
		$subjectModel = new SubjectModel();
		$departmentModel = new DepartmentModel();
		$lstClass = [];
		$subjects = $subjectModel->getByDepartmentID($id);
		$json = json_encode($subjects);
		return $json;
	}
}
