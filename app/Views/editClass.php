<?= $this->extend('_layout') ?>
<?= $this->section('content') ?>

<form method="POST" class="user">

    <div class="form-group">
        <?php if (isset($class)) : ?>
            <input type="text" class="form-control form-control-user" id="name" name="name" placeholder="Nhập tên lớp" value="<?= $class['name'] ?>">
    </div>
    <div class="form-group">
        <input type="text" class="form-control form-control-user" id="code" name="code" placeholder="Nhập mã lớp" value="<?= $class['code'] ?>">
    </div>
    <div class="form-group">
        <input type="text" class="form-control form-control-user" id="numberStudent" name="numberStudent" placeholder="Nhập sĩ số" value="<?= $class['numberStudent'] ?>">
    </div>
    <div class="form-group">
        <input type="date" class="form-control form-control-user" id="timeStart" name="timeStart" value="<?= date('Y-m-d', strtotime($class['timeStart']) == NULL ? time() : strtotime($class['timeStart'])) ?>">
    </div>
    <div class="form-group">
        <input type="date" class="form-control form-control-user" id="timeEnd" name="timeEnd" value="<?= date('Y-m-d', strtotime($class['timeEnd']) == NULL ? time() : strtotime($class['timeEnd'])) ?>">
    </div>
    <?php if (isset($departments)) : ?>
        <div class=" form-group">
            <label class="bold">Khoa</label>
            <select class="form-control first_null not_chosen" name="departmentID" onchange="changeDepartment(this.value)">
                <option value="0">--- Chọn khoa ---</option>

                <?php foreach ($departments as $item) : ?>
                    <option value="<?= $item['id'] ?>" <?php if ($class['departmentID'] == $item['id']) echo 'selected'?> ><?= $item['name'] ?></option>
                <?php endforeach; ?>
            </select>
        </div>
    <?php endif; ?>
        <div class=" form-group">
            <label class="bold">Môn</label>
            <select id="subID" class="form-control first_null not_chosen" name="subID" onchange="changeSubject(this.value)">
                <option value="0">--- Chọn môn ---</option>
            </select>
        </div>
    <?php if (isset($teachers)) : ?>
        <div class=" form-group">
            <label class="bold">Giảng viên</label>
            <select id="teacher" class="form-control first_null not_chosen" name="teacher">
                <option value="0">--- Chọn giảng viên ---</option>
            </select>
        </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-md-6">
            <a href="<?= base_url() . '/home/class' ?>" class="btn btn-primary btn-user btn-block">
                Trở về
            </a>
        </div>
        <div class="col-md-6">
            <button type="submit" class="btn btn-primary btn-user btn-block">
                Xác nhận
            </button>
        </div>
    </div>
<?php endif; ?>
</form>
<script>
    var teacherID = "<?php echo $class['teacherID'] ?>";
    var subID = "<?php echo $class['subID'] ?>";
    async function changeSubject(value) {
        const res = await fetch(`http://localhost/nhom6/home/getTeacherBySubject/${value}`);
        const data = await res.json();
        let html = data.map(el => {
            if (teacherID == el.id) {
                return `<option value="${el.id}" selected>${el.name}</option>`;
            }
            return `<option value="${el.id}">${el.name}</option>`;
        })
        if (!data.length) {

            html = "<option value='0'>--- Chưa có giảng viên phụ trách môn này ---</option>";
        }
        const select = document.getElementById("teacher");
        select.innerHTML = html;
    }
    async function changeDepartment(value) {
        const res = await fetch(`http://localhost/nhom6/home/getSubjectByDepartment/${value}`);
        const data = await res.json();
        let html = data.map(el => {
            if (subID == el.id) {
                return `<option value="${el.id}" selected>${el.name}</option>`;
            }
            return `<option value="${el.id}">${el.name}</option>`;
        })
        if (!data.length) {

            html = "<option value='0'>--- Chưa có môn nào trong khoa ---</option>";
        }
        const select = document.getElementById("subID");
        select.innerHTML = html;
        changeSubject(data[0]?.id);
    }
    changeDepartment(<?= $class['departmentID' ]?>);
    changeSubject(<?= $class['subID' ]?>);
</script>
<?= $this->endSection() ?>