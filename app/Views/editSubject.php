<?= $this->extend('_layout') ?>
<?= $this->section('content') ?>

<form method="POST" class="user">
<?php if (isset($subject)) : ?>

    <div class="form-group">
    <label class="bold">Tên môn học</label>
            <input type="text" class="form-control form-control-user" id="name" name="name" placeholder="Nhập tên môn" value="<?= $subject['name'] ?>">
    </div>
    <?php if (isset($departments)) : ?>
        <div class="form-group">
            <label class="bold">Khoa</label>
            <select class="form-control first_null not_chosen" name="departmentID">
                <option value="0">--- Chọn khoa ---</option>
                <?php foreach ($departments as $item) : ?>
                        <option value="<?= $item['id'] ?>" <?php if ($subject["departmentID"] == $item['id'])  echo 'selected'; ?>><?= $item['name'] ?></option>
                    <?php endforeach; ?>
            </select>
        </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-md-6">
            <a href="<?= base_url() . '/home/subject' ?>" class="btn btn-primary btn-user btn-block">
                Trở về
            </a>
        </div>
        <div class="col-md-6">
            <button type="submit" class="btn btn-primary btn-user btn-block">
                Xác nhận
            </button>
        </div>
    </div>
<?php endif; ?>
</form>

<?= $this->endSection() ?>