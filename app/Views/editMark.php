<?= $this->extend('_layout') ?>
<?= $this->section('content') ?>

<form method="POST" action="<?= base_url() . '/home/editMark?studentID=' . $_GET['studentID'] . '&classID=' . $_GET['classID'] ?>" class="user">
    <div class="form-group">
        <input type="number" step="0.01" min="0" max="10" class="form-control form-control-user" id="gradeAttend" name="gradeAttend" placeholder="Nhập điểm chuyên cần" value="<?= $mark['gradeAttend'] ?>">
    </div>
    <div class="form-group">
        <input type="number" step="0.01" min="0" max="10" class="form-control form-control-user" id="gradeMidTerm" name="gradeMidTerm" placeholder="Nhập điểm thi giữa kỳ" value="<?= $mark['gradeMidTerm'] ?>">
    </div>
    <div class="form-group">
        <input type="number" step="0.01" min="0" max="10" class="form-control form-control-user" id="gradeFinalTerm" name="gradeFinalTerm" placeholder="Nhập điểm thi cuối kỳ" value="<?= $mark['gradeFinalTerm'] ?>">
    </div>
    <div class="row">
        <div class="col-md-6">
            <a href="<?= base_url() . '/home/classes' ?>" class="btn btn-primary btn-user btn-block">
                Trở về
            </a>
        </div>
        <div class="col-md-6">
            <button type="submit" class="btn btn-primary btn-user btn-block">
                Xác nhận
            </button>
        </div>
    </div>
</form>

<?= $this->endSection() ?>